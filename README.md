# IONOS DDNS Updater

This is a simple script to update the DDNS service of IONOS with the ipv6 of the current device.
You have to get your DDNS-Api link from https://developer.hosting.ionos.de/docs/dns .
Further explanation can found at: https://www.ionos.de/hilfe/domains/ip-adresse-konfigurieren/dynamisches-dns-ddns-einrichten-bei-company-name/

For the script to work properly, you have to set the link as an environment variable.
Please run
```
export ionos_link=<your api link>
```

The version of the ip to update can be specified with the following flags
```
-4 Update the ddns with your current ip v4
-6 Update the ddns with your current ip v6
```