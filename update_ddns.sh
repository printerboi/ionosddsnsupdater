#!/bin/bash

v4url="-4"
v6url="-6"
ipurl=""


updateddns(){
    param=$1
    ipurl=$2

    ipaddr=$(curl -s $2 icanhazip.com | xargs echo -n)

    ionoslink=$(printenv IONOS_LINK)

    url="${ionoslink}&${param}=${ipaddr}"

    echo $url

    curl -X GET $url
}

while getopts '46h' opt; do
  case "$opt" in
  4)
      updateddns ipv4 $v4url
      ;;
  6)
      updateddns ipv6 $v6url
      ;;

   ?|h)
      echo "Usage: $(basename $0) [-4] [-6]"
      exit 1
      ;;
  esac
done

